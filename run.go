package artifacts_ilb_harness

import (
	"context"
	"errors"
	"fmt"
	"io"
)

func Run(ctx context.Context, w io.Writer, url string, num int, delayBodyWriting int) (int, error) {
	req := newRequester(num, url)
	req.Run(ctx, delayBodyWriting)

	results, err := req.Results(ctx)
	if err != nil && !(errors.Is(err, context.DeadlineExceeded) || errors.Is(err, context.Canceled)) {
		return 0, fmt.Errorf("collecting results: %w", err)
	}

	errorsNo, err := generateReport(w, results)
	if err != nil {
		return errorsNo, fmt.Errorf("generating report: %w", err)
	}

	return errorsNo, nil
}
