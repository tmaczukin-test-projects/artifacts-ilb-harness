package artifacts_ilb_harness

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"
	"sync"
	"time"
)

const (
	payloadSizeBytes = 10 * 1024 * 1024
)

var (
	payload []byte
)

type result struct {
	err  error
	resp *http.Response
}

type requester struct {
	url     string
	results []result
	wg      *sync.WaitGroup

	doneCh chan struct{}
}

func newRequester(num int, url string) *requester {
	wg := new(sync.WaitGroup)
	wg.Add(num)

	return &requester{
		url:     url,
		results: make([]result, num),
		wg:      wg,
		doneCh:  make(chan struct{}),
	}
}

func (r *requester) Run(ctx context.Context, delayBodyWriting int) {
	for i := range r.results {
		go r.run(ctx, i, delayBodyWriting)
	}

	go r.wait()
}

func (r *requester) run(ctx context.Context, id int, delayBodyWriting int) {
	defer r.wg.Done()

	fmt.Printf("[%d] Running request...\n", id)

	pr, pw := io.Pipe()
	defer pr.Close()

	go func() {
		defer pw.Close()

		if delayBodyWriting > 0 {
			fmt.Printf("[%d] Delay body writing: %d ms\n", id, delayBodyWriting)
			time.Sleep(time.Duration(delayBodyWriting) * time.Millisecond)
		}

		fmt.Printf("[%d] SIZE: %d\n", id, len(payload))

		sent, err := io.Copy(pw, bytes.NewReader(payload))

		fmt.Printf("[%d] COPY ERROR: %v\n", id, err)
		fmt.Printf("[%d] COPY SENT: %d\n", id, sent)
	}()

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, r.url, pr)
	if err != nil {
		fmt.Printf("[%d] Creating request error: %v", id, err)
		return
	}

	fmt.Printf("[%d] executing HTTP request...\n", id)
	resp, rerr := http.DefaultClient.Do(req)
	fmt.Printf("[%d] HTTP request execution finished...\n", id)

	if resp != nil {
		fmt.Printf("[%d] response: %d: %s\n", id, resp.StatusCode, resp.Header.Get(locationHeader))
	} else {
		fmt.Printf("[%d] empty response...\n", id)
	}

	if rerr != nil {
		fmt.Printf("[%d] response error: %v\n", id, rerr)
	} else {
		fmt.Printf("[%d] no response error...\n", id)
	}

	r.results[id].resp = resp
	r.results[id].err = rerr

	fmt.Printf("[%d] Request finished...\n", id)
}

func (r *requester) wait() {
	r.wg.Wait()
	close(r.doneCh)
}

func (r *requester) Results(ctx context.Context) ([]result, error) {
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	case <-r.doneCh:
		return r.results, nil
	}
}

func init() {
	var input []byte

	for c := byte(0x30); c <= 0x39; c++ {
		input = append(input, c)
	}
	for c := byte(0x41); c <= 0x5A; c++ {
		input = append(input, c)
	}
	for c := byte(0x61); c <= 0x7A; c++ {
		input = append(input, c)
	}

	length := len(input)

	blocks := payloadSizeBytes / length
	for i := 0; i < blocks; i++ {
		payload = append(payload, input...)
	}

	rest := payloadSizeBytes % length
	for i := 0; i < rest; i++ {
		payload = append(payload, input[i])
	}
}
