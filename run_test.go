package artifacts_ilb_harness

import (
	"bufio"
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestRequester(t *testing.T) {
	testLocation := "https://example.com/location"

	tests := map[string]int{
		"delay body writing start":       10,
		"begin writing body immediately": 0,
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			ctx, cancelFn := context.WithTimeout(context.Background(), 1*time.Minute)
			defer cancelFn()

			lc := net.ListenConfig{}
			listener, err := lc.Listen(ctx, "tcp", "")
			require.NoError(t, err)

			go serve(t, listener, testLocation)

			parts := strings.Split(listener.Addr().String(), ":")
			port := parts[len(parts)-1]

			url := fmt.Sprintf("http://localhost:%s/", port)

			req := newRequester(20, url)
			req.Run(ctx, tt)

			results, err := req.Results(ctx)
			assert.NoError(t, err)

			for _, result := range results {
				resp := result.resp
				if assert.NotNil(t, resp) {
					assert.Equal(t, http.StatusTemporaryRedirect, resp.StatusCode)
					assert.Equal(t, testLocation, resp.Header.Get(locationHeader))

					t.Logf("Status code: %d", resp.StatusCode)
					t.Logf("Location: %s", resp.Header.Get(locationHeader))
				}

				t.Logf("Error: %v", result.err)
				if !assert.NoError(t, result.err) {
					t.Logf("%s", result.err)
					t.Logf("%#+v", result.err)
				}
			}

			errorsNo, err := generateReport(os.Stdout, results)
			assert.NoError(t, err)
			t.Logf("Errors number: %d", errorsNo)
		})
	}

}

func serve(t *testing.T, listener net.Listener, testLocation string) {
	for {
		conn, err := listener.Accept()
		if assert.NoError(t, err, "listener.Accept()") {
			go handleConnection(t, conn, testLocation)
		}
	}
}

func handleConnection(t *testing.T, conn net.Conn, testLocation string) {
	scanner := bufio.NewReader(conn)

	for {
		l, _, err := scanner.ReadLine()
		assert.NoError(t, err, "reading request line")

		if len(l) == 0 {
			break
		}
	}

	var err error

	_, err = fmt.Fprintf(conn, "HTTP/1.1 307 Temporary Redirect\n")
	assert.NoError(t, err, "writing response status")
	_, err = fmt.Fprintf(conn, "%s: %s\n", locationHeader, testLocation)
	assert.NoError(t, err, "writing response location header")
	_, err = fmt.Fprintln(conn)
	assert.NoError(t, err, "writing response final empty line")

	err = conn.Close()
	assert.NoError(t, err, "closing connection")
}
