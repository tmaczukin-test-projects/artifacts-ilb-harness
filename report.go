package artifacts_ilb_harness

import (
	"encoding/json"
	"fmt"
	"io"
)

const (
	locationHeader = "Location"
)

type report []reportEntry

type reportEntry struct {
	ID               int
	Error            string
	ErrorDetails     string
	ResponseCode     int
	ResponseStatus   string
	ResponseLocation string
}

func generateReport(w io.Writer, results []result) (int, error) {
	errorsNo := 0

	var report report
	for i, result := range results {
		entry := reportEntry{
			ID: i,
		}

		if result.err != nil {
			errorsNo++
			entry.Error = result.err.Error()
			entry.ErrorDetails = fmt.Sprintf("%#+v", result.err)
		}

		if result.resp != nil {
			entry.ResponseCode = result.resp.StatusCode
			entry.ResponseStatus = result.resp.Status
			entry.ResponseLocation = result.resp.Header.Get(locationHeader)
		}

		report = append(report, entry)
	}

	encoder := json.NewEncoder(w)

	err := encoder.Encode(report)
	if err != nil {
		return errorsNo, fmt.Errorf("encoding report: %w", err)
	}

	return errorsNo, nil
}
