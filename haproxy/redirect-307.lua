core.register_service("redirect-307", "http", function(applet)
    applet:set_status(307)
    applet:add_header("location", "http://redirect.example.com" .. applet.path)
    applet:start_response()
    applet:send("")
    applet:receive(1)
end)

