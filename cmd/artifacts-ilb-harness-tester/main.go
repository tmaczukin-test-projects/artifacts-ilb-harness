package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/tmaczukin-test-projects/artifacts-ilb-harness"
)

const (
	envGSTG = "gstg"
	envGPRD = "gprd"

	zoneC = "us-east1-c"
	zoneD = "us-east1-d"

	envFlag              = "env"
	zoneFlag             = "zone"
	helpFlag             = "help"
	timeoutFlag          = "timeout"
	concurrencyFlag      = "concurrency"
	reportFileFlag       = "report-file"
	usePublicFlag        = "use-public"
	delayBodyWritingFlag = "delay-body-writing"
	customURLFlag        = "custom-url"

	defaultTimeoutSeconds   = 60
	defaultConcurrency      = 20
	defaultReportFile       = "./report.json"
	defaultDelayBodyWriting = 0
)

type envZonesILBs map[string]zonesILBs
type zonesILBs map[string]string

var (
	envs  = []string{envGSTG, envGPRD}
	zones = []string{zoneC, zoneD}

	// Mocking job IDs for URL generation. As we will not set any token the
	// artifacts will be not uploaded anyway. The harness tests is purely to
	// check what breaks the connection on the requests that on HaProxy
	// should end with a redirect response.
	jobIDs = map[string]int64{
		envGSTG: 40406933,   // Test job at https://staging.gitlab.com/gitlab-org/test-git.ci-gateway-usage/-/jobs/40406933
		envGPRD: 2127720830, // Test job at https://gitlab.com/gitlab-org/test-git.ci-gateway-usage/-/jobs/2127720830
	}

	publicURLs = map[string]string{
		envGSTG: "https://staging.gitlab.com/gitlab-org/test-git.ci-gateway-usage/-/jobs/40406933",
		envGPRD: "https://gitlab.com/gitlab-org/test-git.ci-gateway-usage/-/jobs/2127720830",
	}

	usePublicFlagVal        = flag.Bool(usePublicFlag, false, "Should the test use the public Interned URL")
	envFlagVal              = flag.String(envFlag, envGSTG, fmt.Sprintf("GitLab environment to use. Available values: %v", envs))
	zoneFlagVal             = flag.String(zoneFlag, zoneC, fmt.Sprintf("GitLab environment to use. Available values: %v", zones))
	helpFlagVal             = flag.Bool(helpFlag, false, "Print usage information")
	timeoutFlagVal          = flag.Int(timeoutFlag, defaultTimeoutSeconds, "Timeout for the harness execution. In seconds")
	concurrencyFlagVal      = flag.Int(concurrencyFlag, defaultConcurrency, "Number of concurrent requests to execute")
	reportFileFlagVal       = flag.String(reportFileFlag, defaultReportFile, "Where to save the JSON encoded report")
	delayBodyWritingFlagVal = flag.Int(delayBodyWritingFlag, defaultDelayBodyWriting, "Whether writing the body (in artifacts case this would be streaming the ZIP archive created in fly) should be delayed. Value in milliseconds. 0 means no delaying.")
	customURLFlagVal        = flag.String(customURLFlag, "", "Use this custom URL instead of one built for specified env/zone")
)

func main() {
	ilbs, err := generateILBList()
	handleError("generating ILB list", err)

	url := handleFlags(ilbs)

	timeCtx, timeCancelFn := context.WithTimeout(context.Background(), time.Duration(*timeoutFlagVal)*time.Second)
	defer timeCancelFn()

	ctx, cancelFn := signal.NotifyContext(timeCtx, syscall.SIGINT, syscall.SIGTERM)
	defer cancelFn()

	fmt.Printf("Environment: %s\n", *envFlagVal)
	fmt.Printf("Zone:        %s\n", *zoneFlagVal)
	fmt.Printf("URL:         %s\n", url)
	fmt.Printf("Report file: %s\n", *reportFileFlagVal)

	file, err := os.Create(*reportFileFlagVal)
	handleError("creating report file", err)

	defer func() {
		_ = file.Close()
	}()

	errorsNo, err := artifacts_ilb_harness.Run(ctx, file, url, *concurrencyFlagVal, *delayBodyWritingFlagVal)
	handleError("running harness test", err)

	fmt.Printf("Number of errors: %d\n", errorsNo)

	errorRate := (float64(errorsNo) / float64(*concurrencyFlagVal)) * 100
	successRate := 100 - errorRate
	fmt.Printf("Success rate: %5.2f%%\n", successRate)
	fmt.Printf("Error rate: %5.2f%%\n", errorRate)

	fmt.Println("Quiting. Bye!")
}

func handleError(context string, err error) {
	if err == nil {
		return
	}

	if context != "" {
		context = context + ": "
	}

	fmt.Printf("[ERROR] %s%v\n", context, err)
	os.Exit(1)
}

func generateILBList() (envZonesILBs, error) {
	ilbs := make(envZonesILBs, 0)

	for _, env := range envs {
		if ilbs[env] == nil {
			ilbs[env] = make(zonesILBs, 0)
		}

		jobID, ok := jobIDs[env]
		if !ok {
			return nil, fmt.Errorf("no jobID defined for %s environment", env)
		}

		for _, zone := range zones {

			ilbs[env][zone] = fmt.Sprintf("https://git-%s.ci-gateway.int.%s.gitlab.net:8989/api/v4/jobs/%d/artifacts", zone, env, jobID)
		}
	}

	return ilbs, nil
}

func handleFlags(ilbs envZonesILBs) string {
	flag.Parse()

	if *helpFlagVal {
		printUsage(ilbs)
		os.Exit(0)
	}

	if *customURLFlagVal != "" {
		return *customURLFlagVal
	}

	if *usePublicFlagVal {
		url, ok := publicURLs[*envFlagVal]
		if !ok {
			handleError("", fmt.Errorf("no public Internet URL defined for %s environment", *envFlagVal))
		}

		return url
	}

	env, ok := ilbs[*envFlagVal]
	if !ok {
		handleError("", fmt.Errorf("no endpoint defined for %s environment", *envFlagVal))
	}

	url, ok := env[*zoneFlagVal]
	if !ok {
		handleError("", fmt.Errorf("no endpoint defined for %s zone", *zoneFlagVal))
	}

	return url
}

func printUsage(ilbs envZonesILBs) {
	flag.Usage()

	fmt.Printf("\nILBs:\n")
	for _, envZones := range ilbs {
		for _, ilb := range envZones {
			fmt.Printf("- %s\n", ilb)
		}
	}
}
