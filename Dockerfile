FROM golang:1.17.7-alpine3.15 AS builder

WORKDIR /src
COPY . /src/

ENV CGO_ENABLED=0

RUN go build -o artifacts-ilb-harness-tester ./cmd/artifacts-ilb-harness-tester

FROM alpine:3.15

RUN apk add --no-cache jq bash tcpdump curl docker-cli-compose

RUN docker compose version

COPY --from=builder /src/artifacts-ilb-harness-tester /usr/local/bin/artifacts-ilb-harness-tester
RUN ln -s /usr/local/bin/artifacts-ilb-harness-tester /usr/local/bin/tester

ENTRYPOINT ["/usr/local/bin/artifacts-ilb-harness-tester"]

